﻿using System;
using System.Windows.Forms;

namespace SystAn
{
    partial class EditProblem
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.AltView = new System.Windows.Forms.DataGridView();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Competent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpView = new System.Windows.Forms.DataGridView();
            this.Expert = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditAltText = new System.Windows.Forms.TextBox();
            this.EditAlt = new System.Windows.Forms.Button();
            this.DeleteAlt = new System.Windows.Forms.Button();
            this.AddAlt = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ExpProb = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddExpToProb = new System.Windows.Forms.Button();
            this.ProbName = new System.Windows.Forms.TextBox();
            this.ProbDes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ClearName = new System.Windows.Forms.Button();
            this.ClearDes = new System.Windows.Forms.Button();
            this.ClearAlt = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.SaveName = new System.Windows.Forms.Button();
            this.SaveDes = new System.Windows.Forms.Button();
            this.FindExp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.AddExp = new System.Windows.Forms.Button();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AltView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpProb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(1813, -8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 43);
            this.button1.TabIndex = 1;
            this.button1.Text = "Закрыть";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AltView
            // 
            this.AltView.AllowUserToAddRows = false;
            this.AltView.AllowUserToDeleteRows = false;
            this.AltView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.AltView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AltView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.Competent});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AltView.DefaultCellStyle = dataGridViewCellStyle1;
            this.AltView.Location = new System.Drawing.Point(12, 199);
            this.AltView.Name = "AltView";
            this.AltView.ReadOnly = true;
            this.AltView.RowHeadersVisible = false;
            this.AltView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AltView.Size = new System.Drawing.Size(804, 724);
            this.AltView.TabIndex = 3;
            this.AltView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AltView_CellClick);
            // 
            // Number
            // 
            this.Number.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Number.HeaderText = "№";
            this.Number.Name = "Number";
            this.Number.ReadOnly = true;
            this.Number.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Number.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Number.Width = 24;
            // 
            // Competent
            // 
            this.Competent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Competent.FillWeight = 500F;
            this.Competent.HeaderText = "Альтернатива";
            this.Competent.Name = "Competent";
            this.Competent.ReadOnly = true;
            this.Competent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ExpView
            // 
            this.ExpView.AllowUserToAddRows = false;
            this.ExpView.AllowUserToDeleteRows = false;
            this.ExpView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExpView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Expert,
            this.Column1,
            this.Column2,
            this.dataGridViewTextBoxColumn1});
            this.ExpView.Location = new System.Drawing.Point(1081, 80);
            this.ExpView.Name = "ExpView";
            this.ExpView.ReadOnly = true;
            this.ExpView.RowHeadersVisible = false;
            this.ExpView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ExpView.Size = new System.Drawing.Size(807, 414);
            this.ExpView.TabIndex = 4;
            this.ExpView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ExpView_CellClick);
            // 
            // Expert
            // 
            this.Expert.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Expert.FillWeight = 500F;
            this.Expert.HeaderText = "ФИО";
            this.Expert.Name = "Expert";
            this.Expert.ReadOnly = true;
            this.Expert.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Expert.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "Область деятельности";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 116;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Контакты";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 62;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.HeaderText = "Компетентность";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 97;
            // 
            // EditAltText
            // 
            this.EditAltText.Location = new System.Drawing.Point(12, 929);
            this.EditAltText.Multiline = true;
            this.EditAltText.Name = "EditAltText";
            this.EditAltText.Size = new System.Drawing.Size(726, 68);
            this.EditAltText.TabIndex = 8;
            this.EditAltText.TextChanged += new System.EventHandler(this.EditAltText_TextChanged);
            // 
            // EditAlt
            // 
            this.EditAlt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.EditAlt.Location = new System.Drawing.Point(12, 1003);
            this.EditAlt.Name = "EditAlt";
            this.EditAlt.Size = new System.Drawing.Size(75, 23);
            this.EditAlt.TabIndex = 14;
            this.EditAlt.Text = "Изменить";
            this.EditAlt.UseVisualStyleBackColor = false;
            this.EditAlt.Click += new System.EventHandler(this.EditAlt_Click);
            // 
            // DeleteAlt
            // 
            this.DeleteAlt.BackColor = System.Drawing.Color.Red;
            this.DeleteAlt.Location = new System.Drawing.Point(174, 1003);
            this.DeleteAlt.Name = "DeleteAlt";
            this.DeleteAlt.Size = new System.Drawing.Size(75, 23);
            this.DeleteAlt.TabIndex = 13;
            this.DeleteAlt.Text = "Удалить";
            this.DeleteAlt.UseVisualStyleBackColor = false;
            this.DeleteAlt.Click += new System.EventHandler(this.DeleteAlt_Click);
            // 
            // AddAlt
            // 
            this.AddAlt.BackColor = System.Drawing.Color.Lime;
            this.AddAlt.Location = new System.Drawing.Point(93, 1003);
            this.AddAlt.Name = "AddAlt";
            this.AddAlt.Size = new System.Drawing.Size(75, 23);
            this.AddAlt.TabIndex = 12;
            this.AddAlt.Text = "Добавить";
            this.AddAlt.UseVisualStyleBackColor = false;
            this.AddAlt.Click += new System.EventHandler(this.AddAlt_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(255, 1003);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(561, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Зафиксировать проблему и список альтернатив";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // ExpProb
            // 
            this.ExpProb.AllowUserToAddRows = false;
            this.ExpProb.AllowUserToDeleteRows = false;
            this.ExpProb.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExpProb.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.Column4,
            this.dataGridViewTextBoxColumn3,
            this.Column3});
            this.ExpProb.Location = new System.Drawing.Point(1081, 557);
            this.ExpProb.Name = "ExpProb";
            this.ExpProb.ReadOnly = true;
            this.ExpProb.RowHeadersVisible = false;
            this.ExpProb.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ExpProb.Size = new System.Drawing.Size(807, 469);
            this.ExpProb.TabIndex = 16;
            this.ExpProb.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ExpProb_CellClick);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.FillWeight = 500F;
            this.dataGridViewTextBoxColumn2.HeaderText = "ФИО";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.HeaderText = "Контакты";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 62;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.HeaderText = "Компетентность";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 97;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.HeaderText = "Оценивание";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 75;
            // 
            // AddExpToProb
            // 
            this.AddExpToProb.Location = new System.Drawing.Point(1411, 500);
            this.AddExpToProb.Name = "AddExpToProb";
            this.AddExpToProb.Size = new System.Drawing.Size(145, 51);
            this.AddExpToProb.TabIndex = 17;
            this.AddExpToProb.Text = "Допустить эксперта к оцениванию";
            this.AddExpToProb.UseVisualStyleBackColor = true;
            this.AddExpToProb.Click += new System.EventHandler(this.AddExpToProb_Click);
            // 
            // ProbName
            // 
            this.ProbName.Location = new System.Drawing.Point(15, 57);
            this.ProbName.Name = "ProbName";
            this.ProbName.Size = new System.Drawing.Size(723, 20);
            this.ProbName.TabIndex = 18;
            // 
            // ProbDes
            // 
            this.ProbDes.Location = new System.Drawing.Point(15, 96);
            this.ProbDes.Multiline = true;
            this.ProbDes.Name = "ProbDes";
            this.ProbDes.Size = new System.Drawing.Size(723, 84);
            this.ProbDes.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(12, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Имя проблемы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Формулировка проблемы";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(12, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Список альтернатив";
            // 
            // ClearName
            // 
            this.ClearName.Location = new System.Drawing.Point(744, 57);
            this.ClearName.Name = "ClearName";
            this.ClearName.Size = new System.Drawing.Size(30, 20);
            this.ClearName.TabIndex = 23;
            this.ClearName.Text = "Х";
            this.ClearName.UseVisualStyleBackColor = true;
            this.ClearName.Click += new System.EventHandler(this.ClearName_Click);
            // 
            // ClearDes
            // 
            this.ClearDes.Location = new System.Drawing.Point(744, 96);
            this.ClearDes.Name = "ClearDes";
            this.ClearDes.Size = new System.Drawing.Size(72, 39);
            this.ClearDes.TabIndex = 24;
            this.ClearDes.Text = "Х";
            this.ClearDes.UseVisualStyleBackColor = true;
            this.ClearDes.Click += new System.EventHandler(this.ClearDes_Click);
            // 
            // ClearAlt
            // 
            this.ClearAlt.Location = new System.Drawing.Point(744, 929);
            this.ClearAlt.Name = "ClearAlt";
            this.ClearAlt.Size = new System.Drawing.Size(72, 68);
            this.ClearAlt.TabIndex = 25;
            this.ClearAlt.Text = "Х";
            this.ClearAlt.UseVisualStyleBackColor = true;
            this.ClearAlt.Click += new System.EventHandler(this.ClearAlt_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(1295, 501);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Компетентность";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 1;
            this.numericUpDown1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown1.Location = new System.Drawing.Point(1300, 517);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(86, 20);
            this.numericUpDown1.TabIndex = 26;
            // 
            // SaveName
            // 
            this.SaveName.Location = new System.Drawing.Point(780, 57);
            this.SaveName.Name = "SaveName";
            this.SaveName.Size = new System.Drawing.Size(36, 20);
            this.SaveName.TabIndex = 28;
            this.SaveName.Text = "V";
            this.SaveName.UseVisualStyleBackColor = true;
            this.SaveName.Click += new System.EventHandler(this.SaveName_Click);
            // 
            // SaveDes
            // 
            this.SaveDes.Location = new System.Drawing.Point(744, 141);
            this.SaveDes.Name = "SaveDes";
            this.SaveDes.Size = new System.Drawing.Size(72, 39);
            this.SaveDes.TabIndex = 29;
            this.SaveDes.Text = "V";
            this.SaveDes.UseVisualStyleBackColor = true;
            this.SaveDes.Click += new System.EventHandler(this.SaveDes_Click);
            // 
            // FindExp
            // 
            this.FindExp.Location = new System.Drawing.Point(1123, 41);
            this.FindExp.Name = "FindExp";
            this.FindExp.Size = new System.Drawing.Size(316, 20);
            this.FindExp.TabIndex = 30;
            this.FindExp.TextChanged += new System.EventHandler(this.FindExp_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(1078, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Поиск";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1562, 500);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(134, 51);
            this.button3.TabIndex = 32;
            this.button3.Text = "Отстранить эксперта от оценивания";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // AddExp
            // 
            this.AddExp.BackColor = System.Drawing.Color.Lime;
            this.AddExp.Location = new System.Drawing.Point(1081, 514);
            this.AddExp.Name = "AddExp";
            this.AddExp.Size = new System.Drawing.Size(75, 23);
            this.AddExp.TabIndex = 33;
            this.AddExp.Text = "Добавить";
            this.AddExp.UseVisualStyleBackColor = false;
            this.AddExp.Click += new System.EventHandler(this.AddExp_Click);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(825, 80);
            this.numericUpDown2.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(78, 20);
            this.numericUpDown2.TabIndex = 34;
            this.numericUpDown2.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(822, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Размер шкалы";
            // 
            // EditProblem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.AddExp);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FindExp);
            this.Controls.Add(this.SaveDes);
            this.Controls.Add(this.SaveName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.ClearAlt);
            this.Controls.Add(this.ClearDes);
            this.Controls.Add(this.ClearName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ProbDes);
            this.Controls.Add(this.ProbName);
            this.Controls.Add(this.AddExpToProb);
            this.Controls.Add(this.ExpProb);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.EditAlt);
            this.Controls.Add(this.DeleteAlt);
            this.Controls.Add(this.AddAlt);
            this.Controls.Add(this.EditAltText);
            this.Controls.Add(this.ExpView);
            this.Controls.Add(this.AltView);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditProblem";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.AltView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpProb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }      

        private void button1_Click(object sender, EventArgs e)
        {
            Index.SelfRef.Render();
            Close();
        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView AltView;
        private System.Windows.Forms.DataGridView ExpView;
        private System.Windows.Forms.TextBox EditAltText;
        private System.Windows.Forms.Button EditAlt;
        private System.Windows.Forms.Button DeleteAlt;
        private System.Windows.Forms.Button AddAlt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView ExpProb;
        private System.Windows.Forms.Button AddExpToProb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn Competent;
        private System.Windows.Forms.TextBox ProbName;
        private System.Windows.Forms.TextBox ProbDes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ClearName;
        private System.Windows.Forms.Button ClearDes;
        private System.Windows.Forms.Button ClearAlt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button SaveName;
        private System.Windows.Forms.Button SaveDes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Expert;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.TextBox FindExp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button button3;
        private Button AddExp;
        private NumericUpDown numericUpDown2;
        private Label label6;
    }
}