﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SystAn
{
    public partial class Index : Form
    {
        public Index()
        {
            InitializeComponent();
            textBox1.Text = "Эксперт1";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            DB.LoadExp();
            try
            {
                var res = DB.Experts.Where(x => x.Name == textBox1.Text).ToArray()[0];
                Eval.expName = textBox1.Text;
                label2.ForeColor = SystemColors.Control;
                
            }
            catch
            {
                label2.ForeColor = Color.Black;
                return;
            }
            Expert a = new Expert(textBox1.Text);
                a.Show();
        }
    }
}
