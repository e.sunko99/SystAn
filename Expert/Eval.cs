﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SystAn
{
    public partial class Eval : Form
    {
        int indProb;
        int indExp;
        int nalt;
        int method;
        public static string expName;
        DB.Problem Problem => DB.Problems[indProb];
        int scale => Problem.Scale;
        DB.Eval eval;
        List<string> Alternatives = new List<string>();
        public Eval(int Prob)
        {
            indProb = Prob;
            DB.LoadAlt(indProb);
            InitializeComponent();
            eval = DB.LoadExpToProb(indProb);
            nalt = eval.nalt;
            indExp = eval.exp.FindIndex(x => x.Name == expName);

            foreach (Control t in Controls)
            {
                t.Size = new Size(t.Size.Width * Width / 1920, t.Size.Height * Height / 1080);
                t.Location = new Point(t.Location.X * Width / 1920, t.Location.Y * Height / 1080);
            }
            var tmp = eval.exp.Find(x => x.Name == expName).PrefEval;
            for (int i = 1; i <= nalt; i++)
            {
                for (int j = 0; j < nalt; j++)
                {
                    if (tmp[j] == i)
                        Alternatives.Add(DB.Alternatives[j].Description);
                }
            }
            foreach (var a in DB.Alternatives)
            {
                if (!Alternatives.Contains(a.Description))
                {
                    Alternatives.Add(a.Description);
                }
            }
            Render();
        }
        public void Render(int method = 1)
        {
            this.method = method;
            eval = DB.LoadExpToProb(indProb);
            textBox1.Text = DB.Problems[indProb].Description;
            nalt = DB.Alternatives.Count;
            switch (method)
            {
                case 1:
                    EvalView.Rows.Clear();
                    for (int i = 0; i < nalt; i++)
                        for (int j = i + 1; j < nalt; j++)
                        {
                            if (eval.PairEval[i][j] == -1 || !checkBox1.Checked)
                            {
                                EvalView.Rows.Add(new object[] { DB.Alternatives[i].Description, "Равноценно", DB.Alternatives[j].Description });
                                for (int k = 0; k < 3; k++)
                                    EvalView.Rows[EvalView.Rows.Count - 1].Cells[k].Style.BackColor = Color.White;
                                if (eval.PairEval[i][j] == 1)
                                    EvalView.Rows[EvalView.Rows.Count - 1].Cells[0].Style.BackColor = Color.LightGreen;
                                if (eval.PairEval[i][j] == 0)
                                    EvalView.Rows[EvalView.Rows.Count - 1].Cells[2].Style.BackColor = Color.LightGreen;
                                if (eval.PairEval[i][j] == 0.5)
                                    EvalView.Rows[EvalView.Rows.Count - 1].Cells[1].Style.BackColor = Color.LightGreen;
                            }
                        }
                    button2.Enabled = ChoosesDone();
                    if (EvalView.Rows.Count > 0)
                        EvalView.Rows[0].Cells[0].Selected = false;
                    break;
                case 2:
                    double sum = 0;
                    WeigView.CellValueChanged -= new DataGridViewCellEventHandler(WeigView_CellEndEdit);
                    WeigView.Rows.Clear();
                    for (int i = 0; i < nalt; i++)
                    {
                        if (eval.exp[indExp].WeigEval[i] >= 0)
                        {
                            WeigView.Rows.Add(new object[] { DB.Alternatives[i].Description, eval.exp[indExp].WeigEval[i] });
                            sum += Convert.ToDouble(WeigView.Rows[i].Cells[1].Value);
                        }
                        else
                            WeigView.Rows.Add(new object[] { DB.Alternatives[i].Description, "0" });
                    }
                    this.sum.ForeColor = Color.Red;
                    this.sum.Text = String.Format("Сумма оценок: {0:0.00}/1", sum);
                    if (sum - 1 < -1e-15)
                    { this.sum.Text += String.Format(" недостает {0:0.00000}", Math.Abs(sum - 1)); }
                    else if (sum - 1 > 1e-15)
                    { this.sum.Text += String.Format(" перебор {0:0.00000}", Math.Abs(sum - 1)); }
                    else
                    {
                        this.sum.ForeColor = Color.Black;
                    }
                    WeigView.CellValueChanged += new DataGridViewCellEventHandler(WeigView_CellEndEdit);
                    break;
                case 3:
                    PrefView.Rows.Clear();
                    for (int i = 0; i < nalt; i++)
                    {
                        PrefView.Rows.Add(new object[] { Alternatives[i], "↑", "↓" });
                    }
                    break;
                case 4:
                    RankView.Rows.Clear();
                    var tmp = eval.exp[indExp].RankEval;
                    for (int i = 0; i < nalt; i++)
                    {
                        if (tmp[i] > 0)
                        {
                            RankView.Rows.Add(new object[] { DB.Alternatives[i].Description, "-", tmp[i].ToString(), "+" });
                        }
                        else
                        {
                            RankView.Rows.Add(new object[] { DB.Alternatives[i].Description, "-", "0", "+" });
                        }
                    }
                    break;
                case 5:
                    FullView.CellValueChanged -= new DataGridViewCellEventHandler(FullView_CellEndEdit);

                    FullView.Rows.Clear();
                    for (int i = 0; i < nalt; i++)
                        for (int j = i + 1; j < nalt; j++)
                        {
                            if (eval.exp[indExp].FullEval[i][j] >= 0)
                            {
                                FullView.Rows.Add(new object[] { DB.Alternatives[i].Description, (int)Math.Round(eval.exp[indExp].FullEval[i][j] * scale), "/" + scale, DB.Alternatives[j].Description });
                            }
                            else
                            {
                                FullView.Rows.Add(new object[] { DB.Alternatives[i].Description, 0, "/" + scale, DB.Alternatives[j].Description });
                            }
                        }
                    button2.Enabled = true;
                    for (int i = 0; i < eval.nalt && button2.Enabled; i++)
                        for (int j = i + 1; j < eval.nalt; j++)
                            button2.Enabled &= eval.exp[indExp].FullEval[i][j] != -1;
                    FullView.CellValueChanged += new DataGridViewCellEventHandler(FullView_CellEndEdit);

                    break;
                default:
                    break;
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        private bool ChoosesDone()
        {
            int res = 0;
            for (int i = 0; i < nalt; i++)
                for (int j = i + 1; j < nalt; j++)
                {
                    if (eval.PairEval[i][j] != -1)
                        res++;
                }
            return res == nalt * (nalt - 1) / 2;
        }

        private void EvalView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            for (int i = 0; i < nalt; i++)
            {
                for (int j = i + 1; j < nalt; j++)
                {
                    if (DB.Alternatives[i].Description == EvalView.Rows[e.RowIndex].Cells[0].Value.ToString() &&
                        DB.Alternatives[j].Description == EvalView.Rows[e.RowIndex].Cells[2].Value.ToString())
                    {
                        DB.eval.PairEval[i][j] = 1 - 0.5 * e.ColumnIndex;
                        DB.eval.PairEval[j][i] = 0.5 * e.ColumnIndex;
                        DB.SaveExpToProb(indProb, eval);
                        button2.Enabled = ChoosesDone();
                        EvalView.CurrentCell = null;
                        for (int k = 0; k < 3; k++)
                            EvalView.Rows[e.RowIndex].Cells[k].Style.BackColor = Color.White;
                        EvalView.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.LightGreen;
                        if (checkBox1.Checked)
                            EvalView.Rows.RemoveAt(e.RowIndex);
                        return;
                    }
                }
            }
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show(
                "После данного действия  оценка данным методом будет зафиксирована\nи дальнейшее редактирование будет невозможно.\nВы уверены что хотите продолжить?",
                "Подтвердите действие", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;
            switch (method)
            {
                case 1:
                    eval.Pair = DB.Condition.Done;
                    break;
                case 2:
                    eval.exp[indExp].Weig = DB.Condition.Done;
                    break;
                case 3:
                    eval.exp[indExp].Pref = DB.Condition.Done;
                    break;
                case 4:
                    eval.exp[indExp].Rank = DB.Condition.Done;
                    break;
                case 5:
                    eval.exp[indExp].Full = DB.Condition.Done;
                    break;
            }

            if (eval.Pair == DB.Condition.Done && eval.exp.FindIndex(x => x.Name == expName &&
               x.Weig == DB.Condition.Done && x.Pref == DB.Condition.Done && x.Full == DB.Condition.Done) > -1)
            {
                eval.exp[indExp].Progress = DB.Condition.Done;
            }
            if (eval.exp.Where(x => x.Progress == DB.Condition.Done).ToList().Count == eval.exp.Count
                && eval.Pair == DB.Condition.Done
                )
                DB.Problems[indProb] = new DB.Problem
                {
                    Name = DB.Problems[indProb].Name,
                    Description = DB.Problems[indProb].Description,
                    condition = DB.Condition.Done
                };
            DB.SaveExpToProb(indProb, eval);
            DB.SaveProb();
            Expert.SelfRef.Render();
            if (eval.exp[indExp].Progress == DB.Condition.Done)
                Close();
        }
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            Render();
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            method = tabControl1.SelectedIndex + 1;
            Render(method);
        }

        private void WeigView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            WeigView.CellValueChanged -= new DataGridViewCellEventHandler(WeigView_CellEndEdit);
            double sum = 0;
            for (int i = 0; i < nalt; i++)
            {
                if (WeigView.Rows[i].Cells[1].Value != null)
                {
                    WeigView.Rows[i].Cells[1].Value = WeigView.Rows[i].Cells[1].Value.ToString().Replace('.', ',');
                    if (WeigView.Rows[i].Cells[1].Value.ToString() == ",")
                    {
                        WeigView.Rows[i].Cells[1].Value = "0,";
                    }
                    string s = WeigView.Rows[i].Cells[1].Value.ToString();                    

                    for (int j = 0; j < s.Length; j++)
                    {
                        if ((s[j] < '0' || s[j] > '9') && s[j] != ',')
                        {
                            s = s.Remove(j--);

                        }
                    }
                    if (s.Length > 0 && s[s.Length - 1] == ',')
                    {
                        s = s.Remove(s.Length - 1, 1);
                    }
                    for (int j = 0; j < s.Length; j++)
                    {
                        for (int k = j + 1; k < s.Length; k++)
                        {
                            if (s[j] == ',' && ',' == s[k])
                            {
                                s = s.Remove(k--);
                            }
                        }
                    }
                    if (s == "")
                        s = "0";
                    double a = Convert.ToDouble(s);
                    sum += a;
                    eval.exp[indExp].WeigEval[i] = a;
                    WeigView.Rows[i].Cells[1].Value = a.ToString();
                }
            }
            this.sum.ForeColor = Color.Red;
            this.sum.Text = String.Format("Сумма оценок: {0:0.00}/1", sum);
            if (sum - 1 < -1e-15)
            { this.sum.Text += String.Format(" недостает {0:0.00000}", Math.Abs(sum - 1)); }
            else if (sum - 1 > 1e-15)
            { this.sum.Text += String.Format(" перебор {0:0.00000}", Math.Abs(sum - 1)); }
            else
            {
                this.sum.ForeColor = Color.Black;
            }
            WeigView.CellValueChanged += new DataGridViewCellEventHandler(WeigView_CellEndEdit);
            if (e.ColumnIndex != 1)
            {
                DB.SaveExpToProb(indProb, eval);
                Render(2);
            }
        }
        private void PrefView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            int j = e.ColumnIndex;
            int n = PrefView.Rows.Count;
            string buffer;
            if (i >= 0 && i < n)
            {
                switch (j)
                {
                    case 1:
                        if (i > 0)
                        {
                            buffer = PrefView.Rows[i].Cells[0].Value.ToString();
                            PrefView.Rows[i].Cells[0].Value = PrefView.Rows[i - 1].Cells[0].Value.ToString();
                            PrefView.Rows[i - 1].Cells[0].Value = buffer;
                        }
                        break;
                    case 2:
                        if (i < n - 1)
                        {
                            buffer = PrefView.Rows[i].Cells[0].Value.ToString();
                            PrefView.Rows[i].Cells[0].Value = PrefView.Rows[i + 1].Cells[0].Value.ToString();
                            PrefView.Rows[i + 1].Cells[0].Value = buffer;
                        }
                        break;
                }
                for (i = 0; i < n; i++)
                {
                    Alternatives[i] = PrefView.Rows[i].Cells[0].Value.ToString();
                    int k = DB.Alternatives.FindIndex(x => x.Description == Alternatives[i]);
                    eval.exp[indExp].PrefEval[k] = i + 1;
                }
                DB.SaveExpToProb(indProb, eval);
            }

        }

        private void RankView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            int j = e.ColumnIndex;
            int n = RankView.Rows.Count;
            if (i >= 0 && i < n)
            {
                switch (j)
                {
                    case 1:
                        RankView[2, i].Value = Math.Max(Convert.ToInt32(RankView[2, i].Value) - 1, 0);
                        break;
                    case 3:
                        RankView[2, i].Value = Math.Min(Convert.ToInt32(RankView[2, i].Value) + 1, 10);
                        break;
                }
                for (i = 0; i < nalt; i++)
                {
                    eval.exp[indExp].RankEval[i] = Convert.ToInt32(RankView[2, i].Value);
                }
                DB.SaveExpToProb(indProb, eval);
            }
        }

        private void FullView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            int l = 0;
            string s;
            FullView.CellValueChanged -= new DataGridViewCellEventHandler(FullView_CellEndEdit);
            if (e.ColumnIndex == 1)
            {
                for (int i = 0; i < eval.nalt; i++)
                    for (int j = i + 1; j < eval.nalt; j++)
                    {
                        s = FullView.Rows[l].Cells[1].Value.ToString();
                        for (int k = 0; k < s.Length; k++)
                        {
                            if (s[k] < '0' || s[k] > '9')
                                s = s.Remove(k--);
                        }
                        int tmp;
                        if (s == "")
                            tmp = 0;
                        else
                            tmp = Math.Min(Convert.ToInt32(s), scale);
                        FullView.Rows[l].Cells[1].Value = tmp;
                        eval.exp[indExp].FullEval[i][j] = (double)tmp / scale;
                        eval.exp[indExp].FullEval[j][i] = 1 - (double)tmp / scale;
                        l++;
                    }
                DB.SaveExpToProb(indProb, eval);
                button2.Enabled = true;
                for (int i = 0; i < eval.nalt && button2.Enabled; i++)
                    for (int j = i + 1; j < eval.nalt; j++)
                        button2.Enabled &= eval.exp[indExp].FullEval[i][j] != -1;
            }
            else
            {
                Render(5);
            }
            FullView.CellValueChanged += new DataGridViewCellEventHandler(FullView_CellEndEdit);
        }
    }
}