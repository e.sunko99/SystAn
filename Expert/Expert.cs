﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SystAn
{
    public partial class Expert : Form
    {
        public static Expert SelfRef;
        int indProb;
        int indExp;
        string exp;
        DB.Eval eval;
        public Expert(string exp)
        {
            InitializeComponent();
            eval = DB.LoadExpToProb(indProb);
            indExp = eval.exp.FindIndex(x=>x.Name==exp);
            foreach (Control t in Controls)
            {
                t.Size = new Size(t.Size.Width * Width / 1920, t.Size.Height * Height / 1080);
                t.Location = new Point(t.Location.X * Width / 1920, t.Location.Y * Height / 1080);
            }
            this.exp = exp;
            SelfRef = this;
            Render();
        }
        public void Render()
        {
            ProbView.Rows.Clear();
            button2.Enabled = false;
            DB.LoadProb();
            int nprob = DB.Problems.Count;
            for (int i = 0; i < nprob; i++)
            {
                eval = DB.LoadExpToProb(i);
                if (eval.exp.FindIndex(x=>x.Name==exp&&x.Progress!=DB.Condition.Done)==-1)
                    continue;
                eval = DB.LoadExpToProb(i);
                if (eval.exp != null && eval.exp.Where(x => x.Name == exp).ToArray().Length > 0)
                {
                    ProbView.Rows.Add(new object[] { DB.Problems[i].Description });
                }
            }
            if (ProbView.Rows.Count == 0)
            {
                button2.Enabled = false;
                ProbView.Rows.Add(new object[] { "Нет проблем, готовых к оцениванию" });
            }
            else
            {
                button2.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ProbView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            for (int i = 0; i < DB.Problems.Count; i++)
            {
                if (DB.Problems[i].Description == ProbView.Rows[e.RowIndex].Cells[0].Value.ToString())
                {
                    indProb = i;
                }
            }
            for (int i = 0; i < ProbView.Rows.Count; i++)
            {
                ProbView.Rows[i].Cells[0].Style.BackColor = Color.White;
            }            
            ProbView.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.LightGreen;
            ProbView.CurrentCell = null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(indProb!=-1)
            {
                var a = new Eval(indProb);
                a.Show();
            }
        }
    }
}
