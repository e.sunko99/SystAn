﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SystAn
{
    public partial class Edit_Expert : Form
    {
        int indExp;
        bool edit;
        bool show;
        int indProb = -1;
        DB.Eval eval=new DB.Eval();
        public Edit_Expert(int exp)
        {
            DB.LoadExp();
            indExp = exp;
            edit = exp > -1;
            show = exp == -2;
            if (exp < 0)
                indExp = DB.Experts.Count - 1;
            InitializeComponent();
            int Width = SystemInformation.PrimaryMonitorSize.Width;
            int Height = SystemInformation.PrimaryMonitorSize.Height;
            foreach (Control t in Controls)
            {
                t.Size = new Size(t.Size.Width * Width / 1920, t.Size.Height * Height / 1080);
                t.Location = new Point(t.Location.X * Width / 1920, t.Location.Y * Height / 1080);
            }
            Render();
        }
        public  void Render()
        {
            ExpName.Text = DB.Experts[indExp].Name;
            Contacts.Text = DB.Experts[indExp].Contacts;
            Competent.Value = Convert.ToDecimal(DB.Experts[indExp].competent);
            Info.Text = DB.Experts[indExp].Info;
            FieldOfActivity.Text = DB.Experts[indExp].FieldOfActivity;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!edit && !show)
            {
                DB.DelExp(DB.Experts.Count - 1);
            }
            Index.SelfRef.Render();
            Close();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            DB.EditExp(indExp, ExpName.Text, Convert.ToDouble(Competent.Value), FieldOfActivity.Text, Contacts.Text, Info.Text);
            DB.SaveExp();

            if (indProb != -1)
            {
                eval = DB.LoadExpToProb(indProb);
                DB.ExpToProb exp = new DB.ExpToProb
                {
                    Name = ExpName.Text,
                    competent = Convert.ToDouble(Competent.Value),
                    Progress = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    Weig = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    WeigEval = DB.EvalVector(DB.Alternatives.Count),
                    Pref = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    PrefEval = DB.EvalVectorInt(DB.Alternatives.Count),
                    Rank = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    RankEval = DB.EvalVectorInt(DB.Alternatives.Count),
                    Full = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    FullEval = DB.EvalMatrix(DB.Alternatives.Count)
                };
                if (eval.exp == null)
                {
                    eval.exp = new List<DB.ExpToProb>();
                }
                eval.exp.Add(exp);
                DB.SaveExpToProb(indProb,eval);
            }
            if (EditProblem.SelfRef != null)
                EditProblem.SelfRef.Render();
            Index.SelfRef.Render();
            Close();
        }
        
    }
}
