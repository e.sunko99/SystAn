﻿using System.Drawing;
using System.Windows.Forms;

namespace SystAn
{
    partial class Index
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CloseButton = new System.Windows.Forms.Button();
            this.ProbView = new System.Windows.Forms.DataGridView();
            this.Problems = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpView = new System.Windows.Forms.DataGridView();
            this.Res = new System.Windows.Forms.Button();
            this.AddProb = new System.Windows.Forms.Button();
            this.DeleteExp = new System.Windows.Forms.Button();
            this.EditProb = new System.Windows.Forms.Button();
            this.AddExp = new System.Windows.Forms.Button();
            this.FindProb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.FindExp = new System.Windows.Forms.TextBox();
            this.EditExp = new System.Windows.Forms.Button();
            this.DelExp = new System.Windows.Forms.Button();
            this.Expert = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Competent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ProbView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpView)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.Color.Red;
            this.CloseButton.Location = new System.Drawing.Point(1813, -8);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(94, 43);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "Закрыть";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.Close_Click);
            // 
            // ProbView
            // 
            this.ProbView.AllowUserToAddRows = false;
            this.ProbView.AllowUserToDeleteRows = false;
            this.ProbView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ProbView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProbView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Problems,
            this.Status});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProbView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProbView.Location = new System.Drawing.Point(12, 70);
            this.ProbView.Name = "ProbView";
            this.ProbView.ReadOnly = true;
            this.ProbView.RowHeadersVisible = false;
            this.ProbView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ProbView.Size = new System.Drawing.Size(807, 998);
            this.ProbView.TabIndex = 2;
            this.ProbView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ProbView_CellClick);
            // 
            // Problems
            // 
            this.Problems.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Problems.HeaderText = "Проблемы";
            this.Problems.Name = "Problems";
            this.Problems.ReadOnly = true;
            this.Problems.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Problems.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Status.HeaderText = "Статус";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Status.Width = 47;
            // 
            // ExpView
            // 
            this.ExpView.AllowUserToAddRows = false;
            this.ExpView.AllowUserToDeleteRows = false;
            this.ExpView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExpView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Expert,
            this.Column2,
            this.Column1,
            this.Competent});
            this.ExpView.Location = new System.Drawing.Point(1081, 66);
            this.ExpView.Name = "ExpView";
            this.ExpView.ReadOnly = true;
            this.ExpView.RowHeadersVisible = false;
            this.ExpView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ExpView.Size = new System.Drawing.Size(807, 1002);
            this.ExpView.TabIndex = 3;
            this.ExpView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ExpView_CellClick);
            // 
            // Res
            // 
            this.Res.Location = new System.Drawing.Point(825, 156);
            this.Res.Name = "Res";
            this.Res.Size = new System.Drawing.Size(75, 49);
            this.Res.TabIndex = 6;
            this.Res.Text = "Результаты оценки";
            this.Res.UseVisualStyleBackColor = true;
            this.Res.Click += new System.EventHandler(this.Res_Click);
            // 
            // AddProb
            // 
            this.AddProb.BackColor = System.Drawing.Color.Lime;
            this.AddProb.Location = new System.Drawing.Point(825, 98);
            this.AddProb.Name = "AddProb";
            this.AddProb.Size = new System.Drawing.Size(75, 23);
            this.AddProb.TabIndex = 9;
            this.AddProb.Text = "Добавить";
            this.AddProb.UseVisualStyleBackColor = false;
            this.AddProb.Click += new System.EventHandler(this.AddProb_Click);
            // 
            // DeleteExp
            // 
            this.DeleteExp.BackColor = System.Drawing.Color.Red;
            this.DeleteExp.Location = new System.Drawing.Point(825, 127);
            this.DeleteExp.Name = "DeleteExp";
            this.DeleteExp.Size = new System.Drawing.Size(75, 23);
            this.DeleteExp.TabIndex = 10;
            this.DeleteExp.Text = "Удалить";
            this.DeleteExp.UseVisualStyleBackColor = false;
            this.DeleteExp.Click += new System.EventHandler(this.DeleteProb_Click);
            // 
            // EditProb
            // 
            this.EditProb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.EditProb.Location = new System.Drawing.Point(825, 70);
            this.EditProb.Name = "EditProb";
            this.EditProb.Size = new System.Drawing.Size(75, 23);
            this.EditProb.TabIndex = 11;
            this.EditProb.Text = "Изменить";
            this.EditProb.UseVisualStyleBackColor = false;
            this.EditProb.Click += new System.EventHandler(this.EditProb_Click);
            // 
            // AddExp
            // 
            this.AddExp.BackColor = System.Drawing.Color.Lime;
            this.AddExp.Location = new System.Drawing.Point(1000, 98);
            this.AddExp.Name = "AddExp";
            this.AddExp.Size = new System.Drawing.Size(75, 23);
            this.AddExp.TabIndex = 20;
            this.AddExp.Text = "Добавить";
            this.AddExp.UseVisualStyleBackColor = false;
            this.AddExp.Click += new System.EventHandler(this.AddExp_Click);
            // 
            // FindProb
            // 
            this.FindProb.Location = new System.Drawing.Point(607, 40);
            this.FindProb.Name = "FindProb";
            this.FindProb.Size = new System.Drawing.Size(212, 20);
            this.FindProb.TabIndex = 23;
            this.FindProb.TextChanged += new System.EventHandler(this.FindProb_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(562, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Поиск";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(1079, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Поиск";
            // 
            // FindExp
            // 
            this.FindExp.Location = new System.Drawing.Point(1124, 40);
            this.FindExp.Name = "FindExp";
            this.FindExp.Size = new System.Drawing.Size(212, 20);
            this.FindExp.TabIndex = 25;
            this.FindExp.TextChanged += new System.EventHandler(this.FindExp_TextChanged);
            // 
            // EditExp
            // 
            this.EditExp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.EditExp.Location = new System.Drawing.Point(1000, 70);
            this.EditExp.Name = "EditExp";
            this.EditExp.Size = new System.Drawing.Size(75, 23);
            this.EditExp.TabIndex = 22;
            this.EditExp.Text = "Изменить";
            this.EditExp.UseVisualStyleBackColor = false;
            this.EditExp.Click += new System.EventHandler(this.EditExp_Click);
            // 
            // DelExp
            // 
            this.DelExp.BackColor = System.Drawing.Color.Red;
            this.DelExp.Location = new System.Drawing.Point(1000, 127);
            this.DelExp.Name = "DelExp";
            this.DelExp.Size = new System.Drawing.Size(75, 23);
            this.DelExp.TabIndex = 27;
            this.DelExp.Text = "Удалить";
            this.DelExp.UseVisualStyleBackColor = false;
            this.DelExp.Click += new System.EventHandler(this.DelExp_Click);
            // 
            // Expert
            // 
            this.Expert.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Expert.FillWeight = 500F;
            this.Expert.HeaderText = "ФИО";
            this.Expert.Name = "Expert";
            this.Expert.ReadOnly = true;
            this.Expert.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Expert.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Область Деятельности";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 119;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "Контакты";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 62;
            // 
            // Competent
            // 
            this.Competent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Competent.HeaderText = "Компетентность";
            this.Competent.Name = "Competent";
            this.Competent.ReadOnly = true;
            this.Competent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Competent.Width = 97;
            // 
            // Index
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.DelExp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FindExp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FindProb);
            this.Controls.Add(this.EditExp);
            this.Controls.Add(this.AddExp);
            this.Controls.Add(this.EditProb);
            this.Controls.Add(this.DeleteExp);
            this.Controls.Add(this.AddProb);
            this.Controls.Add(this.Res);
            this.Controls.Add(this.ExpView);
            this.Controls.Add(this.ProbView);
            this.Controls.Add(this.CloseButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Index";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ProbView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.DataGridView ProbView;
        private System.Windows.Forms.DataGridView ExpView;
        private System.Windows.Forms.Button Res;
        private Button AddProb;
        private Button DeleteExp;
        private Button EditProb;
        private Button AddExp;
        private DataGridViewTextBoxColumn Problems;
        private DataGridViewTextBoxColumn Status;
        private TextBox FindProb;
        private Label label1;
        private Label label2;
        private TextBox FindExp;
        private Button EditExp;
        private Button DelExp;
        private DataGridViewTextBoxColumn Expert;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Competent;
    }
}

