﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
namespace SystAn
{
    public partial class EditProblem : Form
    {
        int indProb;
        int indAlt;
        int indExp;
        bool edit;
        bool canedit;
        bool show;        
        DB.Eval eval;
        public static EditProblem SelfRef; 
        public EditProblem(int Prob)
        {
            SelfRef = this;
            indProb = Prob;
            edit = (indProb != -1);
            if (!edit)
            {
                DB.AddProb("", "", 10);
                indProb = DB.Problems.Count - 1;
            }
            canedit = DB.Problems[indProb].condition == DB.Condition.NotExpert;
            show = DB.Problems[indProb].condition == DB.Condition.Done;
            InitializeComponent();
            numericUpDown2.Value = DB.Problems[indProb].Scale;
            int Width = SystemInformation.PrimaryMonitorSize.Width;
            int Height = SystemInformation.PrimaryMonitorSize.Height;
            foreach (Control t in Controls)
            {
                t.Size = new Size(t.Size.Width * Width / 1920, t.Size.Height * Height / 1080);
                t.Location = new Point(t.Location.X * Width / 1920, t.Location.Y * Height / 1080);
            }
            Render();
        }
        public void Render()
        {
            button3.Enabled = false;
            button3.Visible = false;
            if (!canedit || show)
            {
                ProbName.Enabled = false;
                ProbDes.Enabled = false;
                ClearDes.Enabled = false;
                ClearName.Enabled = false;
                AddAlt.Enabled = false;
                DeleteAlt.Enabled = false;
                EditAlt.Enabled = false;
                EditAltText.Enabled = false;
                AltView.Enabled = false;
                SaveDes.Enabled = false;
                SaveName.Enabled = false;
                ClearAlt.Enabled = false;
                numericUpDown2.Enabled = false;
            }
            if (show)
            {
                ExpView.Enabled = false;
                numericUpDown1.Enabled = false;                
                ExpProb.Enabled = false;
                AddExpToProb.Enabled = false;
            }
            ProbName.Text = DB.Problems[indProb].Name;
            ProbDes.Text = DB.Problems[indProb].Description;
            AddExpToProb.Enabled = false;
            ExpView.Rows.Clear();
            eval = DB.LoadExpToProb(indProb);
            DB.LoadExp();
            foreach (var exp in DB.Experts)
            {
                if (eval.exp == null || (!eval.exp.Exists(t => t.Name == exp.Name) && exp.Name.ToLower().Contains(FindExp.Text.ToLower())))
                    ExpView.Rows.Add(new object[] { exp.Name, exp.FieldOfActivity, exp.Contacts, exp.competent });
            }
            AltView.Rows.Clear();
            int i = 0;
            eval.nalt = DB.LoadAlt(indProb);
            foreach (DB.Alternative Alt in DB.Alternatives)
            {
                i++;
                AltView.Rows.Add(new object[] { i, Alt.Description });
            }
            AltView.CurrentCell = null;
            EditAlt.Enabled = false;
            AddAlt.Enabled = false;
            DeleteAlt.Enabled = false;
            ExpView.CurrentCell = null;
            indAlt = -1;
            EditAltText.Text = "";
            ExpProb.Rows.Clear();
            if (eval.exp != null)
            {
                foreach (var exp in eval.exp)
                {
                    int index = DB.Experts.FindIndex(t => t.Name == exp.Name);
                    string s;
                    switch (exp.Progress)
                    {
                        case DB.Condition.NotExpert:
                            s = "Не начато";
                            break;
                        case DB.Condition.Expert:
                            s = "Начато";
                            break;
                        default:
                            s = "Закончено";
                            break;
                    }
                    ExpProb.Rows.Add(new object[] { exp.Name, DB.Experts[index].Contacts, exp.competent, s });
                }
                button2.Enabled = (eval.exp.Count > 0 && DB.Alternatives.Count > 1);
            }
            else { button2.Enabled = false; }
        }
        private void AltView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            indAlt = e.RowIndex;
            for (int i = 0; i < AltView.Rows.Count; i++)
            {
                for (int j = 0; j < AltView.Rows[i].Cells.Count; j++)
                    AltView.Rows[i].Cells[j].Style.BackColor = Color.White;
            }
            for (int j = 0; j < AltView.Rows[e.RowIndex].Cells.Count; j++)
                AltView.Rows[e.RowIndex].Cells[j].Style.BackColor = Color.LightGreen;
            AltView.CurrentCell = null;
            EditAlt.Enabled = (EditAltText.Text != "");
            DeleteAlt.Enabled = true;
        }
        private void ExpView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            indExp = e.RowIndex;
            numericUpDown1.Value = Convert.ToDecimal(ExpView.Rows[e.RowIndex].Cells[3].Value);
            for (int i = 0; i < ExpView.Rows.Count; i++)
            {
                for (int j = 0; j < ExpView.Rows[i].Cells.Count; j++)
                    ExpView.Rows[i].Cells[j].Style.BackColor = Color.White;
            }
            for (int j = 0; j < ExpView.Rows[e.RowIndex].Cells.Count; j++)
                ExpView.Rows[e.RowIndex].Cells[j].Style.BackColor = Color.LightGreen;
            ExpView.CurrentCell = null;
            AddExpToProb.Text = "Допустить эксперта к оцениванию";
            button3.Enabled = false;
            button3.Visible = false;
            if (eval.exp == null || eval.exp.Where(x => x.Name == ExpView.Rows[e.RowIndex].Cells[0].Value.ToString()).ToArray().Length == 0)
                AddExpToProb.Enabled = true;
            else
                AddExpToProb.Enabled = false;
        }
        private void EditAlt_Click(object sender, EventArgs e)
        {
            DB.EditAlt(indAlt, indProb, EditAltText.Text);
            Render();
        }
        private void AddAlt_Click(object sender, EventArgs e)
        {
            DB.AddAlt(indProb, EditAltText.Text);
            Render();
        }
        private void EditAltText_TextChanged(object sender, EventArgs e)
        {
            EditAlt.Enabled = (EditAltText.Text != "" && indAlt != -1);
            AddAlt.Enabled = (EditAltText.Text != "");
        }
        private void DeleteAlt_Click(object sender, EventArgs e)
        {
            DB.DelAlt(indProb, indAlt);
            Render();
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show(
            "После данного действия, редактирование списка альтернатив и формулировки данной проблемы будет недоступно. Вы уверены?",
            "Подтвердите действие", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;
            DB.Problems[indProb] = new DB.Problem
            {
                Name = DB.Problems[indProb].Name,
                Description = DB.Problems[indProb].Description,
                condition = DB.Condition.Expert,
                Scale = Convert.ToInt32(numericUpDown2.Value)
            };
            DB.SaveProb();
            Index.SelfRef.Render();
            Close();
        }
        private void AddExpToProb_Click(object sender, EventArgs e)
        {
            if (AddExpToProb.Text == "Допустить эксперта к оцениванию")
            {
                DB.ExpToProb exp = new DB.ExpToProb
                {
                    Name = ExpView.Rows[indExp].Cells[0].Value.ToString(),
                    competent = Convert.ToDouble(numericUpDown1.Value),
                    Progress = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    Weig = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    WeigEval = DB.EvalVector(eval.nalt),
                    Pref = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    PrefEval = DB.EvalVectorInt(eval.nalt),
                    Rank = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    RankEval = DB.EvalVectorInt(eval.nalt),
                    Full = (DB.Condition)Enum.GetValues(typeof(DB.Condition)).GetValue(0),
                    FullEval = DB.EvalMatrix(eval.nalt)
                };
                if (eval.exp == null)
                {
                    eval.exp = new List<DB.ExpToProb>();
                }
                eval.exp.Add(exp);                
            }
            else
            {
                DB.ExpToProb exp = new DB.ExpToProb
                {
                    Name = eval.exp[indExp].Name,
                    competent = Convert.ToDouble(numericUpDown1.Value),
                    Progress = eval.exp[indExp].Progress,
                    Weig = eval.exp[indExp].Weig,
                    WeigEval = eval.exp[indExp].WeigEval,
                    Pref = eval.exp[indExp].Pref,
                    PrefEval = eval.exp[indExp].PrefEval,
                    Rank = eval.exp[indExp].Rank,
                    RankEval = eval.exp[indExp].RankEval,
                    Full = eval.exp[indExp].Full,                    
                    FullEval = eval.exp[indExp].FullEval
                };
                eval.exp[indExp] = exp;
            }
            DB.SaveExpToProb(indProb, eval);
            Render();
        }
        private void ClearName_Click(object sender, EventArgs e)
        {
            ProbName.Text = DB.Problems[indProb].Name;
        }
        private void ClearDes_Click(object sender, EventArgs e)
        {
            ProbDes.Text = DB.Problems[indProb].Description;
        }
        private void ClearAlt_Click(object sender, EventArgs e)
        {
            EditAltText.Text = "";
        }
        private void SaveName_Click(object sender, EventArgs e)
        {
            DB.EditProb(indProb, ProbName.Text, DB.Problems[indProb].Description);
        }
        private void SaveDes_Click(object sender, EventArgs e)
        {
            DB.EditProb(indProb, DB.Problems[indProb].Name, ProbDes.Text);
        }
        private void ExpProb_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            numericUpDown1.Value = Convert.ToDecimal(ExpProb.Rows[e.RowIndex].Cells[2].Value);
            for (int i = 0; i < ExpProb.Rows.Count; i++)
            {
                for (int j = 0; j < ExpProb.Rows[i].Cells.Count; j++)
                    ExpProb.Rows[i].Cells[j].Style.BackColor = Color.White;
            }
            for (int j = 0; j < ExpProb.Rows[e.RowIndex].Cells.Count; j++)
                ExpProb.Rows[e.RowIndex].Cells[j].Style.BackColor = Color.LightGreen;
            indExp = e.RowIndex;
            ExpProb.CurrentCell = null;
            AddExpToProb.Text = "Изменить оценку компетентности";
            AddExpToProb.Enabled = true;
            button3.Enabled = true;
            button3.Visible = true;
        }

        private void FindExp_TextChanged(object sender, EventArgs e)
        {
            Render();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ExpProb.Rows[indExp].Cells[3].Value.ToString() != "Не начато")
                return;
            eval.exp.RemoveAt(indExp);
            DB.SaveExpToProb(indProb, eval);
            Render();
        }

        private void AddExp_Click(object sender, EventArgs e)
        {
            DB.AddExp("", 0, "", "", "");
            Edit_Expert a = new Edit_Expert(-1);
            a.Show();
        }
    }
}