﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SystAn
{
    public partial class ShowResult : Form
    {        
        public ShowResult(int Prob)
        {
            InitializeComponent();
            foreach (Control t in Controls)
            {
                t.Size = new Size(t.Size.Width * Width / 1920, t.Size.Height * Height / 1080);
                t.Location = new Point(t.Location.X * Width / 1920, t.Location.Y * Height / 1080);
            }
            Render(Prob);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void Render(int Prob)
        {
            DB.LoadAlt(Prob);
            int n = DB.Alternatives.Count;
            //0
            defView.Rows.Clear();
            for(int i = 0; i < n; i++)
            {
                defView.Rows.Add(new object[] { (i + 1).ToString() + ") "+ DB.Alternatives[i].Description });
            }
            DB.Eval eval = DB.LoadExpToProb(Prob);
            //1
            DB.PairCompare(eval);
            PairView.Rows.Clear();            
            List<int> places = new List<int>();
            for (int i = 0; i < n; i++)
                places.Add(1);
            List<double> choosed=new List<double>();
            for (int i = 0; i < n; i++)
            {
                choosed.Clear();
                for (int j = 0; j < n; j++)
                    if (DB.Alternatives[i].PairCompare < DB.Alternatives[j].PairCompare)
                    {
                        if (!choosed.Contains(DB.Alternatives[j].PairCompare))
                            places[i]++;
                        choosed.Add(DB.Alternatives[j].PairCompare);

                    }                
            }
            for (int i = 0; i < n; i++)
            {
               PairView.Rows.Add(new object[] { places[i], (i + 1).ToString() + ") " + DB.Alternatives[i].Description, Math.Round(DB.Alternatives[i].PairCompare,3) });
            }
            PairView.Sort(PairView.Columns[0], ListSortDirection.Ascending);
            //2
            DB.WeightExp(eval);
            WeigView.Rows.Clear();
            places.Clear();
            for (int i = 0; i < n; i++)
                places.Add(1);
            for (int i = 0; i < n; i++)
            {
                choosed.Clear();
                for (int j = 0; j < n; j++)
                    if (DB.Alternatives[i].WeightExp < DB.Alternatives[j].WeightExp)
                    {
                        if (!choosed.Contains(DB.Alternatives[j].WeightExp))
                            places[i]++;
                        choosed.Add(DB.Alternatives[j].WeightExp);

                    }
            }
            for (int i = 0; i < n; i++)
            {
                WeigView.Rows.Add(new object[] { places[i], (i + 1).ToString()+") "+ DB.Alternatives[i].Description, Math.Round(DB.Alternatives[i].WeightExp, 3) });
            }
            WeigView.Sort(WeigView.Columns[0], ListSortDirection.Ascending);
            //3
            DB.PreferenceMethod(eval);
            PrefView.Rows.Clear();
            places = new List<int>();
            for (int i = 0; i < n; i++)
                places.Add(1);
            for (int i = 0; i < n; i++)
            {
                choosed.Clear();
                for (int j = 0; j < n; j++)
                    if (DB.Alternatives[i].PreferenceMethod < DB.Alternatives[j].PreferenceMethod)
                    {
                        if (!choosed.Contains(DB.Alternatives[j].PreferenceMethod))
                            places[i]++;
                        choosed.Add(DB.Alternatives[j].PreferenceMethod);

                    }
            }
            for (int i = 0; i < n; i++)
            {
                PrefView.Rows.Add(new object[] { places[i], (i + 1).ToString() + ") " +  DB.Alternatives[i].Description, Math.Round(DB.Alternatives[i].PreferenceMethod, 3) });
            }
            PrefView.Sort(PrefView.Columns[0], ListSortDirection.Ascending);
            //4
            DB.RankMethod(eval);
            RankView.Rows.Clear();
           places = new List<int>();
            for (int i = 0; i < n; i++)
                places.Add(1);
            for (int i = 0; i < n; i++)
            {
                choosed.Clear();
                for (int j = 0; j < n; j++)
                    if (DB.Alternatives[i].RankMethod < DB.Alternatives[j].RankMethod)
                    {
                        if (!choosed.Contains(DB.Alternatives[j].RankMethod))
                            places[i]++;
                        choosed.Add(DB.Alternatives[j].RankMethod);

                    }
            }
            for (int i = 0; i < n; i++)
            {
                RankView.Rows.Add(new object[] { places[i], (i + 1).ToString() + ") " + DB.Alternatives[i].Description, Math.Round(DB.Alternatives[i].RankMethod, 3) });
            }
            RankView.Sort(RankView.Columns[0], ListSortDirection.Ascending);
            //5
            DB.FullPairComparsion(eval);
            FullView.Rows.Clear();
            places = new List<int>();
            for (int i = 0; i < n; i++)
                places.Add(1);
            for (int i = 0; i < n; i++)
            {
                choosed.Clear();
                for (int j = 0; j < n; j++)
                    if (DB.Alternatives[i].FullPairComparsion < DB.Alternatives[j].FullPairComparsion)
                    {
                        if (!choosed.Contains(DB.Alternatives[j].FullPairComparsion))
                            places[i]++;
                        choosed.Add(DB.Alternatives[j].FullPairComparsion);

                    }
            }
            for (int i = 0; i < n; i++)
            {
                FullView.Rows.Add(new object[] { places[i], (i + 1).ToString() + ") " + DB.Alternatives[i].Description, Math.Round(DB.Alternatives[i].FullPairComparsion, 3) });
            }
            FullView.Sort(FullView.Columns[0], ListSortDirection.Ascending);
        }
    }
}
