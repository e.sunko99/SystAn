﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace SystAn
{
    static class DB
    {
        public class Problem
        {
            public string Name;
            public string Description;
            public int Scale=10;
            public Condition condition { get; set; }
        }
        public enum Condition
        {
            NotExpert,
            Expert,
            Done
        }
        public class Expert
        {
            public string Name;
            public string FieldOfActivity;
            public string Contacts;
            public string Info;
            public double competent;
        }
        public class ExpToProb
        {
            public string Name;
            public double competent;
            public Condition Progress { set; get; }
            public Condition Weig { set; get; }
            public double[] WeigEval;
            public Condition Pref { set; get; }
            public int[] PrefEval { set; get; }
            public Condition Rank { set; get; }
            public int[] RankEval { set; get; }
            public Condition Full { set; get; }
            public double[][] FullEval { set; get; }
        }
        public class Eval
        {
            public int nalt;
            public int nexp;
            public Condition Pair;
            public double[][] PairEval;
            public List<ExpToProb> exp { set; get; }
        }
        public class Alternative
        {
            public string Description;
            public double PairCompare;
            public double WeightExp;
            public double PreferenceMethod;
            public double RankMethod;
            public double FullPairComparsion;
        }
        public static List<Problem> Problems = new List<Problem>();
        public static List<Alternative> Alternatives = new List<Alternative>();
        public static List<Expert> Experts = new List<Expert>();
        public static Eval eval;
        public static void LoadProb()
        {
            using (StreamReader sr = new StreamReader("../../../Problems/Problems.txt"))
            {
                int n = Convert.ToInt32(sr.ReadLine());
                Problems.Clear();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                for (int i = 0; i < n; i++)
                {
                    Problems.Add(serializer.Deserialize<Problem>(sr.ReadLine()));
                }
            }
        }
        public static void SaveProb()
        {
            using (StreamWriter sw = new StreamWriter("../../../Problems/Problems.txt"))
            {
                sw.WriteLine(Problems.Count);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                for (int i = 0; i < Problems.Count; i++)
                {
                    sw.WriteLine(serializer.Serialize(Problems[i]));
                }
            }
        }
        public static int LoadAlt(int Prob)
        {
            int n;
            try
            {
                using (StreamReader sr = new StreamReader(String.Format("../../../Problems/{0}/alternatives.txt", Prob)))
                {
                    n = Convert.ToInt32(sr.ReadLine());
                    Alternatives.Clear();
                    for (int i = 0; i < n; i++)
                    {
                        Alternatives.Add(new Alternative
                        {
                            Description = sr.ReadLine(),
                            PairCompare = 0,
                            PreferenceMethod = 0,
                            RankMethod = 0,
                            WeightExp = 0,
                            FullPairComparsion = 0
                        });
                    }
                }
            }
            catch
            {
                Alternatives.Clear();
                SaveAlt(Prob);
                return LoadAlt(Prob);
            }
            return n;
        }
        public static void SaveAlt(int Prob)
        {
            using (StreamWriter sw = new StreamWriter(String.Format("../../../Problems/{0}/alternatives.txt", Prob)))
            {
                sw.WriteLine(Alternatives.Count);
                for (int i = 0; i < Alternatives.Count; i++)
                {
                    sw.WriteLine(Alternatives[i].Description);
                }
            }
        }
        public static void LoadExp()
        {
            using (StreamReader sr = new StreamReader("../../../Experts/Experts.txt"))
            {
                int n = Convert.ToInt32(sr.ReadLine());
                Experts.Clear();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                for (int i = 0; i < n; i++)
                {
                    Experts.Add(serializer.Deserialize<Expert>(sr.ReadLine()));
                }
            }
        }
        public static double[] EvalVector(int nalt)
        {
            double[] res = new double[nalt];
            for (int i = 0; i < nalt; i++)
                res[i] = -1;
            return res;
        }
        public static int[] EvalVectorInt(int nalt)
        {
            int[] res = new int[nalt];
            for (int i = 0; i < nalt; i++)
                res[i] = -1;
            return res;
        }
        public static double[][] EvalMatrix(int nalt)
        {
            double[][] res = new double[nalt][];
            for (int i = 0; i < nalt; i++)
            {
                res[i] = new double[nalt];
                for (int j = 0; j < nalt; j++)
                    res[i][j] = -1;
                res[i][i] = 0;
            }
            return res;
        }
        public static void SaveExpToProb(int Prob, Eval eval)
        {
            eval.nalt = LoadAlt(Prob);
            if (eval.PairEval == null || eval.PairEval.Length != eval.nalt)
            {
                eval.PairEval = EvalMatrix(eval.nalt);
            }
            if (eval.exp != null)
            {
                for (int i = 0; i < eval.exp.Count; i++)
                {
                    if (eval.exp[i].WeigEval == null)
                    {
                        eval.exp[i] = new ExpToProb
                        {
                            Name = eval.exp[i].Name,
                            competent = eval.exp[i].competent,
                            Progress = eval.exp[i].Progress,
                            Weig = eval.exp[i].Weig,
                            WeigEval = EvalVector(eval.nalt),
                            Pref = eval.exp[i].Pref,
                            PrefEval = EvalVectorInt(eval.nalt),
                            Rank = eval.exp[i].Rank,
                            RankEval = EvalVectorInt(eval.nalt),
                            Full = eval.exp[i].Full,
                            FullEval = EvalMatrix(eval.nalt)
                        };
                    }
                }
            }
            using (StreamWriter sw = new StreamWriter(String.Format("../../../Problems/{0}/experts.txt", Prob)))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                sw.WriteLine(serializer.Serialize(eval));
            }
        }
        public static Eval LoadExpToProb(int Prob)
        {
            using (StreamReader sr = new StreamReader(String.Format("../../../Problems/{0}/experts.txt", Prob)))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                eval = serializer.Deserialize<Eval>(sr.ReadToEnd());
            }
            return eval;
        }
    }
}