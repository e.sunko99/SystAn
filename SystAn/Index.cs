﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SystAn
{
    public partial class Index : Form
    {
        int indProb;
        int indExp;
        public static Index SelfRef { get; set; }
        public Index()
        {
            SelfRef = this;
            InitializeComponent();
            int Width = SystemInformation.PrimaryMonitorSize.Width;
            int Height = SystemInformation.PrimaryMonitorSize.Height;
            foreach (Control t in Controls)
            {
                t.Size = new Size(t.Size.Width * Width / 1920, t.Size.Height * Height / 1080);
                t.Location = new Point(t.Location.X * Width / 1920, t.Location.Y * Height / 1080);
            }
            Render();
        }
        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void Render()
        {
            ProbView.Rows.Clear();
            DB.LoadProb();
            foreach (DB.Problem Prob in DB.Problems)
            {
                string status;
                if (Prob.condition == DB.Condition.NotExpert)
                {
                    status = "Оценивание не начато";
                }
                else if (Prob.condition == DB.Condition.Expert)
                {
                    status = "В процессе оценивания";
                }
                else
                {
                    status = "Оценивание завершено";
                }
                if (FindProb.Text == "" || Prob.Name.ToLower().Contains(FindProb.Text.ToLower()))
                    ProbView.Rows.Add(new object[] { Prob.Name, status });
            }
            if (ProbView.Rows.Count > 0)
                ProbView.CurrentCell = ProbView.Rows[0].Cells[0];
            ProbView.CurrentCell = null;
            ExpView.Rows.Clear();
            DB.LoadExp();
            foreach (var exp in DB.Experts)
            {
                if (FindExp.Text == "" || (exp.Name+exp.FieldOfActivity).ToLower().Contains(FindExp.Text.ToLower()))
                    ExpView.Rows.Add(new object[] { exp.Name,exp.FieldOfActivity,exp.Contacts, exp.competent });
            }
            ExpView.CurrentCell = null;
            indProb = -1;
            EditProb.Enabled = false;
            DeleteExp.Enabled = false;
            EditExp.Enabled = false;
            indExp = -1;
            DelExp.Enabled = false;
            Res.Enabled = false;
        }
        private void ProbView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;            
            indProb = DB.Problems.FindIndex(prob => prob.Name == ProbView.Rows[e.RowIndex].Cells[0].Value.ToString());
            for (int i = 0; i < ProbView.Rows.Count; i++)
            {
                ProbView.Rows[i].Cells[0].Style.BackColor = Color.White;
                ProbView.Rows[i].Cells[1].Style.BackColor = Color.White;
            }
            ProbView.Rows[e.RowIndex].Cells[0].Style.BackColor = Color.LightGreen;
            ProbView.Rows[e.RowIndex].Cells[1].Style.BackColor = Color.LightGreen;
            ProbView.CurrentCell = null;
            EditProb.Enabled = true;
            DeleteExp.Enabled = false;
            Res.Enabled = false;
            EditProb.Text = "Изменить";
            if (DB.Problems[indProb].condition == DB.Condition.NotExpert)
            {                
                DeleteExp.Enabled = true;
            }
            else if (DB.Problems[indProb].condition == DB.Condition.Expert)
            {             
            }
            else
            {
                Res.Enabled = true;
                EditProb.Text = "Просмотр";
            }
        }
        private void ExpView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            int columns = ExpView.Columns.Count;
            for (int i = 0; i < ExpView.Rows.Count; i++)
            {
                for (int j = 0; j < columns; j++)
                    ExpView.Rows[i].Cells[j].Style.BackColor = Color.White;
            }
            for (int j = 0; j < columns; j++)
                ExpView.Rows[e.RowIndex].Cells[j].Style.BackColor = Color.LightGreen;
            ExpView.CurrentCell = null;
            indExp = DB.Experts.FindIndex(exp => exp.Name == ExpView.Rows[e.RowIndex].Cells[0].Value.ToString());
            EditExp.Enabled = true;
            DelExp.Enabled = true;
        }
        private void Res_Click(object sender, EventArgs e)
        {
            ShowResult a = new ShowResult(indProb);
            a.Show();
        }
        private void EditProb_Click(object sender, EventArgs e)
        {
            EditProblem a = new EditProblem(indProb);
            a.Show();
        }
        private void AddProb_Click(object sender, EventArgs e)
        {            
            EditProblem a = new EditProblem(-1);
            a.Show();
        }

        private void DeleteProb_Click(object sender, EventArgs e)
        {
            DB.DelProb(indProb);
            Render();
        }
        private void AddExp_Click(object sender, EventArgs e)
        {
            DB.AddExp("", 0, "", "", "");
            Edit_Expert a = new Edit_Expert(-1);
            a.Show();
        }
        
        private void EditExp_Click(object sender, EventArgs e)
        {
            Edit_Expert a = new Edit_Expert(indExp);
            a.Show();
        }

        private void FindProb_TextChanged(object sender, EventArgs e)
        {
            Render();
        }

        private void FindExp_TextChanged(object sender, EventArgs e)
        {
            Render();
        }

        private void DelExp_Click(object sender, EventArgs e)
        {
            DB.DelExp(indExp);
            Render();
        }
    }
}