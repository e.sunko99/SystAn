﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace SystAn
{
    static class DB
    {
        public class Problem
        {
            public string Name;
            public string Description;
            public int Scale;
            public Condition condition;
        }
        public enum Condition
        {
            NotExpert,
            Expert,
            Done
        }
        public class Expert
        {
            public string Name;
            public string FieldOfActivity;
            public string Contacts;
            public string Info;
            public double competent;
        }
        public class ExpToProb
        {
            public string Name;
            public double competent;
            public Condition Progress;
            public Condition Weig;
            public double[] WeigEval;
            public Condition Pref;
            public int[] PrefEval;
            public Condition Rank;
            public int[] RankEval;
            public Condition Full;
            public double[][] FullEval;

        }
        public class Eval
        {
            public int nalt;
            public int nexp { get { if (exp != null) return exp.Count; return 0; } }
            public Condition Pair;
            public double[][] PairEval;
            public List<ExpToProb> exp;
        }
        public class Alternative
        {
            public string Description;
            public double PairCompare;
            public double WeightExp;
            public double PreferenceMethod;
            public double RankMethod;
            public double FullPairComparsion;
        }
        public static List<Problem> Problems = new List<Problem>();
        public static List<Alternative> Alternatives = new List<Alternative>();
        public static List<Expert> Experts = new List<Expert>();
        public static Eval eval;
        //загрузка списка проблем
        public static void LoadProb()
        {
            using (StreamReader sr = new StreamReader("../../../Problems/Problems.txt"))
            {
                int n = Convert.ToInt32(sr.ReadLine());
                Problems.Clear();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                for (int i = 0; i < n; i++)
                {
                    Problems.Add(serializer.Deserialize<Problem>(sr.ReadLine()));
                }
            }
        }
        //сохранение списка проблем
        public static void SaveProb()
        {
            using (StreamWriter sw = new StreamWriter("../../../Problems/Problems.txt"))
            {
                sw.WriteLine(Problems.Count);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                for (int i = 0; i < Problems.Count; i++)
                {
                    sw.WriteLine(serializer.Serialize(Problems[i]));
                }
            }
        }
        //добавление проблемы в список проблем
        public static void AddProb(string Name, string Desc,int scale)
        {
            Problems.Add(new Problem
            {
                Name = Name,
                Description = Desc,
                condition = Condition.NotExpert,
                Scale=scale
            });
            SaveProb();
            Directory.CreateDirectory(String.Format("../../../Problems/{0}", Problems.Count - 1));
            Alternatives.Clear();
            SaveAlt(Problems.Count - 1);
        }
        //удаление проблемы
        public static void DelProb(int index)
        {
            Problems.RemoveAt(index);
            Directory.Delete(String.Format("../../../Problems/{0}", index), true);
            for (int i = index; i < Problems.Count; i++)
            {
                Directory.Move(String.Format("../../../Problems/{0}", index + 1), String.Format("../../../Problems/{0}", index));
            }
            SaveProb();
        }
        //изменение формулировки и имени проблем
        public static void EditProb(int index, string Name, string Desc)
        {
            Problems[index] = new Problem
            {
                Name = Name,
                Description = Desc,
                condition = Condition.NotExpert
            };
            SaveProb();
        }
        //загрузка списка альтернатив 
        public static int LoadAlt(int Prob)
        {
            int n;
            try
            {
                using (StreamReader sr = new StreamReader(String.Format("../../../Problems/{0}/alternatives.txt", Prob)))
                {
                    n = Convert.ToInt32(sr.ReadLine());
                    Alternatives.Clear();
                    for (int i = 0; i < n; i++)
                    {
                        Alternatives.Add(new Alternative
                        {
                            Description = sr.ReadLine(),
                            PairCompare = 0,
                            PreferenceMethod = 0,
                            RankMethod = 0,
                            WeightExp = 0,
                            FullPairComparsion = 0
                        });
                    }
                }
            }
            catch
            {
                Alternatives.Clear();
                SaveAlt(Prob);
                return LoadAlt(Prob);
            }
            return n;
        }
        public static void SaveAlt(int Prob)
        {
            using (StreamWriter sw = new StreamWriter(String.Format("../../../Problems/{0}/alternatives.txt", Prob)))
            {
                sw.WriteLine(Alternatives.Count);
                for (int i = 0; i < Alternatives.Count; i++)
                {
                    sw.WriteLine(Alternatives[i].Description);
                }
            }
        }
        public static void AddAlt(int Prob, string Alt)
        {
            Alternatives.Add(new Alternative
            {
                Description = Alt,
                PairCompare = 0,
                PreferenceMethod = 0,
                RankMethod = 0,
                WeightExp = 0,
                FullPairComparsion = 0
            });
            SaveAlt(Prob);
        }
        public static void DelAlt(int Prob, int index)
        {
            Alternatives.RemoveAt(index);
            SaveAlt(Prob);
        }
        public static void EditAlt(int index, int Prob, string Alt)
        {
            Alternatives[index] = new Alternative
            {
                Description = Alt,
                PairCompare = 0,
                PreferenceMethod = 0,
                RankMethod = 0,
                WeightExp = 0,
                FullPairComparsion = 0
            };
            SaveAlt(Prob);
        }
        public static void LoadExp()
        {
            using (StreamReader sr = new StreamReader("../../../Experts/Experts.txt"))
            {
                int n = Convert.ToInt32(sr.ReadLine());
                Experts.Clear();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                for (int i = 0; i < n; i++)
                {
                    Experts.Add(serializer.Deserialize<Expert>(sr.ReadLine()));
                }
            }
        }
        public static void SaveExp()
        {
            using (StreamWriter sw = new StreamWriter("../../../Experts/Experts.txt"))
            {
                sw.WriteLine(Experts.Count);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                for (int i = 0; i < Experts.Count; i++)
                {
                    sw.WriteLine(serializer.Serialize(Experts[i]));
                }
            }
        }
        public static void AddExp(string Name, double Comp, string Field, string Contact, string Info)
        {
            Experts.Add(new Expert
            {
                Name = Name,
                competent = Comp,
                FieldOfActivity = Field,
                Contacts = Contact,
                Info = Info
            });
            SaveExp();
        }
        public static void DelExp(int index)
        {
            Experts.RemoveAt(index);
            SaveExp();
        }
        public static void EditExp(int index, string Name, double Comp, string Field, string Contact, string Info)
        {
            Experts[index] = new Expert
            {
                Name = Name,
                competent = Comp,
                FieldOfActivity = Field,
                Contacts = Contact,
                Info = Info
            };
            SaveExp();
        }        
        public static double[] EvalVector(int nalt)
        {
            double[] res = new double[nalt];
            for (int i = 0; i < nalt; i++)
                res[i] = -1;
            return res;
        }
        public static int[] EvalVectorInt(int nalt)
        {
            int[] res = new int[nalt];
            for (int i = 0; i < nalt; i++)
                res[i] = -1;
            return res;
        }
        public static double[][] EvalMatrix(int nalt)
        {
            double[][] res = new double[nalt][];
            for (int i = 0; i < nalt; i++)
            {
                res[i] = new double[nalt];
                for (int j = 0; j < nalt; j++)
                    res[i][j] = -1;
                res[i][i] = 0;
            }
            return res;
        }
        public static void PairCompare(Eval eval)
        {
            if (eval.Pair != Condition.Done)
            {
                for (int i = 0; i < eval.nalt; i++)
                {
                    Alternatives[i] = new Alternative
                    {
                        Description = Alternatives[i].Description,
                        PairCompare = 0
                    };
                }
            }
            int R = (eval.nalt * (eval.nalt - 1)) / 2;
            for (int i = 0; i < eval.nalt; i++)
            {
                for (int j = 0; j < eval.nalt; j++)
                {
                    if (i != j)
                        Alternatives[i] = new Alternative
                        {
                            Description = Alternatives[i].Description,
                            PairCompare = Alternatives[i].PairCompare + eval.PairEval[i][j] / R
                        };
                }
            }
        }
        public static void WeightExp(Eval eval)
        {
            for (int i = 0; i < eval.nalt; i++)
            {
                Alternatives[i] = new Alternative
                {
                    Description = Alternatives[i].Description,
                    WeightExp = 0
                };
            }
            for (int exp = 0; exp < eval.nexp; exp++)
            {
                if (eval.exp[exp].Weig == Condition.Done)
                {
                    for (int i = 0; i < eval.nalt; i++)
                    {
                        if (eval.exp[exp].WeigEval[i] >= 0)
                            Alternatives[i] = new Alternative
                            {
                                Description = Alternatives[i].Description,
                                WeightExp = Alternatives[i].WeightExp + eval.exp[exp].WeigEval[i] * eval.exp[exp].competent
                            };
                    }
                }
            }
            double R = 0;
            for (int i = 0; i < eval.nalt; i++)
            {
                R += Alternatives[i].WeightExp;
            }
            for (int i = 0; i < eval.nalt; i++)
            {
                Alternatives[i] = new Alternative
                {
                    Description = Alternatives[i].Description,
                    WeightExp = Alternatives[i].WeightExp / R
                };
            }
        }
        public static void PreferenceMethod(Eval eval)
        {
            for (int i = 0; i < eval.nalt; i++)
            {
                Alternatives[i] = new Alternative
                {
                    Description = Alternatives[i].Description,
                    PreferenceMethod = 0
                };
            }
            for (int exp = 0; exp < eval.nexp; exp++)
            {
                if (eval.exp[exp].Pref == Condition.Done)
                {
                    for (int i = 0; i < eval.nalt; i++)
                    {
                        Alternatives[i] = new Alternative
                        {
                            Description = Alternatives[i].Description,
                            PreferenceMethod = Alternatives[i].PreferenceMethod + eval.nalt - eval.exp[exp].PrefEval[i]
                        };
                    }
                }
            }
            double L = 0;
            for (int i = 0; i < eval.nalt; i++)
            {
                L += Alternatives[i].PreferenceMethod;
            }
            for (int i = 0; i < eval.nalt; i++)
            {
                Alternatives[i] = new Alternative
                {
                    Description = Alternatives[i].Description,
                    PreferenceMethod = Alternatives[i].PreferenceMethod / L
                };
            }
        }
        public static void RankMethod(Eval eval)
        {
            double[] S = new double[eval.nexp];
            for (int exp = 0; exp < eval.nexp; exp++)
            {
                S[exp] = 0;
                if (eval.exp[exp].Rank == Condition.Done)
                    for (int alt = 0; alt < eval.nalt; alt++)
                    {
                        S[exp] += eval.exp[exp].RankEval[alt];
                    }
            }
            double[,] tmp = new double[eval.nexp, eval.nalt];
            for (int exp = 0; exp < eval.nexp; exp++)
            {
                if (eval.exp[exp].Rank == Condition.Done)
                    for (int alt = 0; alt < eval.nalt; alt++)
                    {
                        tmp[exp, alt] = eval.exp[exp].RankEval[alt] / S[exp];
                    }
            }
            for (int exp = 0; exp < eval.nexp; exp++)
            {
                if (eval.exp[exp].Rank == Condition.Done)
                    for (int alt = 0; alt < eval.nalt; alt++)
                    {
                        Alternatives[alt] = new Alternative
                        {
                            Description = Alternatives[alt].Description,
                            RankMethod = Alternatives[alt].RankMethod + tmp[exp, alt] / eval.nexp
                        };
                    }
            }
        }
        public static void FullPairComparsion(Eval eval)
        {
            int n = eval.nalt;
            int N = n * (n - 1) / 2;
            double[,] f = new double[eval.nexp, n];
            for (int k = 0; k < eval.nexp; k++)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (i != j)
                            f[k, i] += eval.exp[k].FullEval[i][j] / N / eval.nexp;
                    }
                }
            }
            for(int i = 0; i < n; i++)
            {
                Alternatives[i].FullPairComparsion = 0;
                for (int k = 0; k < eval.nexp; k++)
                    Alternatives[i].FullPairComparsion += f[k, i];
            }
        }
        public static void SaveExpToProb(int Prob, Eval eval)
        {
            eval.nalt = LoadAlt(Prob);
            if (eval.PairEval == null || eval.PairEval.Length != eval.nalt)
            {
                eval.PairEval = EvalMatrix(eval.nalt);
            }
            if (eval.exp != null)
            {
                for (int i = 0; i < eval.exp.Count; i++)
                {
                    if (eval.exp[i].WeigEval == null)
                    {
                        eval.exp[i] = new ExpToProb
                        {
                            Name = eval.exp[i].Name,
                            competent = eval.exp[i].competent,
                            Progress = eval.exp[i].Progress,
                            Weig = eval.exp[i].Weig,
                            WeigEval = EvalVector(eval.nalt),
                            Pref = eval.exp[i].Pref,
                            PrefEval = EvalVectorInt(eval.nalt),
                            Rank = eval.exp[i].Rank,
                            RankEval = EvalVectorInt(eval.nalt),
                            Full = eval.exp[i].Full,
                            FullEval = EvalMatrix(eval.nalt)
                        };
                    }
                }
            }
            using (StreamWriter sw = new StreamWriter(String.Format("../../../Problems/{0}/experts.txt", Prob)))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                sw.WriteLine(serializer.Serialize(eval));
            }
        }
        public static Eval LoadExpToProb(int Prob)
        {
            try
            {
                using (StreamReader sr = new StreamReader(String.Format("../../../Problems/{0}/experts.txt", Prob)))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    eval = serializer.Deserialize<Eval>(sr.ReadToEnd());
                }
            }
            catch
            {
                SaveExpToProb(Prob, new Eval());
                return LoadExpToProb(Prob);
            }
            return eval;
        }
    }
}