﻿namespace SystAn
{
    partial class Eval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Eval));
            this.EvalView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.sum = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.WeigView = new System.Windows.Forms.DataGridView();
            this.WeighAlt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.PrefView = new System.Windows.Forms.DataGridView();
            this.PrefAlt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefUp = new System.Windows.Forms.DataGridViewButtonColumn();
            this.PrefDown = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.RankView = new System.Windows.Forms.DataGridView();
            this.RankAlt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Minus = new System.Windows.Forms.DataGridViewButtonColumn();
            this.RankEval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plus = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.FullView = new System.Windows.Forms.DataGridView();
            this.FullLeft = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullEval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FulScale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullRight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EvalView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WeigView)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PrefView)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RankView)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FullView)).BeginInit();
            this.SuspendLayout();
            // 
            // EvalView
            // 
            this.EvalView.AllowUserToAddRows = false;
            this.EvalView.AllowUserToDeleteRows = false;
            this.EvalView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.EvalView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EvalView.ColumnHeadersVisible = false;
            this.EvalView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EvalView.DefaultCellStyle = dataGridViewCellStyle4;
            this.EvalView.Location = new System.Drawing.Point(6, 36);
            this.EvalView.Name = "EvalView";
            this.EvalView.ReadOnly = true;
            this.EvalView.RowHeadersVisible = false;
            this.EvalView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.EvalView.Size = new System.Drawing.Size(1880, 903);
            this.EvalView.TabIndex = 0;
            this.EvalView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.EvalView_CellClick);
            this.EvalView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.EvalView_CellClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 5;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(1798, -7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 43);
            this.button1.TabIndex = 5;
            this.button1.Text = "Закрыть";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 25);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1780, 66);
            this.textBox1.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1798, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 40);
            this.button2.TabIndex = 7;
            this.button2.Text = "Окончательная оценка";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Формулировка Проблемы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(490, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(910, 26);
            this.label2.TabIndex = 9;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.checkBox1.Location = new System.Drawing.Point(6, 6);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(305, 17);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Скрывать пары альтернатив с совершённым выбором";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 97);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1896, 971);
            this.tabControl1.TabIndex = 11;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.EvalView);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1888, 945);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Попарное сравнение";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.sum);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.WeigView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1888, 945);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Взвешенных экспертных оценок";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // sum
            // 
            this.sum.AutoSize = true;
            this.sum.Location = new System.Drawing.Point(6, 3);
            this.sum.Name = "sum";
            this.sum.Size = new System.Drawing.Size(103, 13);
            this.sum.TabIndex = 13;
            this.sum.Text = "Сумма оценок: 0/1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(318, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(898, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Для оценивания альтернатив вводите число от 0 до 1 (чем предпочтительней альтерна" +
    "тива тем больше), так чтобы в сумме была 1, левее находится значение суммы оцено" +
    "к";
            // 
            // WeigView
            // 
            this.WeigView.AllowUserToAddRows = false;
            this.WeigView.AllowUserToDeleteRows = false;
            this.WeigView.AllowUserToResizeColumns = false;
            this.WeigView.AllowUserToResizeRows = false;
            this.WeigView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WeigView.ColumnHeadersVisible = false;
            this.WeigView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WeighAlt,
            this.Column5});
            this.WeigView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.WeigView.Location = new System.Drawing.Point(6, 19);
            this.WeigView.Name = "WeigView";
            this.WeigView.RowHeadersVisible = false;
            this.WeigView.Size = new System.Drawing.Size(1870, 920);
            this.WeigView.TabIndex = 0;
            this.WeigView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.WeigView_CellEndEdit);
            // 
            // WeighAlt
            // 
            this.WeighAlt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.WeighAlt.HeaderText = "Column4";
            this.WeighAlt.Name = "WeighAlt";
            this.WeighAlt.ReadOnly = true;
            this.WeighAlt.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WeighAlt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WeighAlt.Width = 5;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.PrefView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1888, 945);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Метод Предпочтений";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(458, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(742, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Расположите альтернативы в порядке убывания их предпочтительности(верхняя наиболе" +
    "е предпочтительная), используя кнопки со стрелками";
            // 
            // PrefView
            // 
            this.PrefView.AllowUserToAddRows = false;
            this.PrefView.AllowUserToDeleteRows = false;
            this.PrefView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PrefView.ColumnHeadersVisible = false;
            this.PrefView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PrefAlt,
            this.PrefUp,
            this.PrefDown});
            this.PrefView.Location = new System.Drawing.Point(6, 19);
            this.PrefView.Name = "PrefView";
            this.PrefView.ReadOnly = true;
            this.PrefView.RowHeadersVisible = false;
            this.PrefView.Size = new System.Drawing.Size(1870, 920);
            this.PrefView.TabIndex = 0;
            this.PrefView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PrefView_CellContentClick);
            // 
            // PrefAlt
            // 
            this.PrefAlt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PrefAlt.HeaderText = "Column4";
            this.PrefAlt.Name = "PrefAlt";
            this.PrefAlt.ReadOnly = true;
            this.PrefAlt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PrefAlt.Width = 5;
            // 
            // PrefUp
            // 
            this.PrefUp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PrefUp.HeaderText = "Column4";
            this.PrefUp.Name = "PrefUp";
            this.PrefUp.ReadOnly = true;
            this.PrefUp.Width = 5;
            // 
            // PrefDown
            // 
            this.PrefDown.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PrefDown.HeaderText = "Column4";
            this.PrefDown.Name = "PrefDown";
            this.PrefDown.ReadOnly = true;
            this.PrefDown.Width = 5;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.RankView);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1888, 945);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Метод Ранга";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // RankView
            // 
            this.RankView.AllowUserToAddRows = false;
            this.RankView.AllowUserToDeleteRows = false;
            this.RankView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RankView.ColumnHeadersVisible = false;
            this.RankView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RankAlt,
            this.Minus,
            this.RankEval,
            this.Plus});
            this.RankView.Location = new System.Drawing.Point(6, 19);
            this.RankView.Name = "RankView";
            this.RankView.ReadOnly = true;
            this.RankView.RowHeadersVisible = false;
            this.RankView.Size = new System.Drawing.Size(1870, 920);
            this.RankView.TabIndex = 0;
            this.RankView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RankView_CellContentClick);
            // 
            // RankAlt
            // 
            this.RankAlt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RankAlt.HeaderText = "Column4";
            this.RankAlt.Name = "RankAlt";
            this.RankAlt.ReadOnly = true;
            this.RankAlt.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.RankAlt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RankAlt.Width = 5;
            // 
            // Minus
            // 
            this.Minus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Minus.HeaderText = "Column4";
            this.Minus.Name = "Minus";
            this.Minus.ReadOnly = true;
            this.Minus.Width = 5;
            // 
            // RankEval
            // 
            this.RankEval.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RankEval.HeaderText = "Column4";
            this.RankEval.Name = "RankEval";
            this.RankEval.ReadOnly = true;
            this.RankEval.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RankEval.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RankEval.Width = 5;
            // 
            // Plus
            // 
            this.Plus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Plus.HeaderText = "Column4";
            this.Plus.Name = "Plus";
            this.Plus.ReadOnly = true;
            this.Plus.Width = 5;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Controls.Add(this.FullView);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1888, 945);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Полного Попарного сопоставления";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // FullView
            // 
            this.FullView.AllowUserToAddRows = false;
            this.FullView.AllowUserToDeleteRows = false;
            this.FullView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FullView.ColumnHeadersVisible = false;
            this.FullView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FullLeft,
            this.FullEval,
            this.FulScale,
            this.FullRight});
            this.FullView.Location = new System.Drawing.Point(6, 19);
            this.FullView.Name = "FullView";
            this.FullView.RowHeadersVisible = false;
            this.FullView.Size = new System.Drawing.Size(1870, 920);
            this.FullView.TabIndex = 0;
            this.FullView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.FullView_CellEndEdit);
            // 
            // FullLeft
            // 
            this.FullLeft.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FullLeft.HeaderText = "Column4";
            this.FullLeft.Name = "FullLeft";
            this.FullLeft.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FullEval
            // 
            this.FullEval.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FullEval.HeaderText = "Column4";
            this.FullEval.Name = "FullEval";
            this.FullEval.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FullEval.Width = 5;
            // 
            // FulScale
            // 
            this.FulScale.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FulScale.HeaderText = "Column4";
            this.FulScale.Name = "FulScale";
            this.FulScale.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FulScale.Width = 5;
            // 
            // FullRight
            // 
            this.FullRight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FullRight.HeaderText = "Column4";
            this.FullRight.Name = "FullRight";
            this.FullRight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(240, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(1454, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(762, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(456, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Выставите оценки от 0 до 10 для каждой из альтернатив списка испольлуя кнопки + и" +
    " -";
            // 
            // Eval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Eval";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Eval";
            ((System.ComponentModel.ISupportInitialize)(this.EvalView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WeigView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PrefView)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RankView)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FullView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView EvalView;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView WeigView;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label sum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn WeighAlt;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridView PrefView;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefAlt;
        private System.Windows.Forms.DataGridViewButtonColumn PrefUp;
        private System.Windows.Forms.DataGridViewButtonColumn PrefDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView RankView;
        private System.Windows.Forms.DataGridViewTextBoxColumn RankAlt;
        private System.Windows.Forms.DataGridViewButtonColumn Minus;
        private System.Windows.Forms.DataGridViewTextBoxColumn RankEval;
        private System.Windows.Forms.DataGridViewButtonColumn Plus;
        private System.Windows.Forms.DataGridView FullView;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullLeft;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullEval;
        private System.Windows.Forms.DataGridViewTextBoxColumn FulScale;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullRight;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}